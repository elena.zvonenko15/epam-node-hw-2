const auth = require('../controllers/auth.controller.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyUsername = require('../middlewares/verifyUsername.js');

module.exports = (app) => {
  app.post('/api/auth/register', requestLogger, verifyUsername, auth.register);

  app.post('/api/auth/login', requestLogger, auth.login);
};
