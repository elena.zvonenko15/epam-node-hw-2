const notes = require('../controllers/note.controller.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyToken = require('../middlewares/verifyToken.js');

module.exports = (app) => {
  app.get('/api/notes', requestLogger, verifyToken, notes.getAll);

  app.post('/api/notes', requestLogger, verifyToken, notes.create);

  app.get('/api/notes/:noteId', requestLogger, verifyToken, notes.getOne);

  app.put('/api/notes/:noteId', requestLogger, verifyToken, notes.update);

  app.patch('/api/notes/:noteId', requestLogger, verifyToken, notes.toggle);

  app.delete('/api/notes/:noteId', requestLogger, verifyToken, notes.delete);
};
