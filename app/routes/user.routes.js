const user = require('../controllers/user.controller.js');
const requestLogger = require('../middlewares/requestLogger.js');
const verifyToken = require('../middlewares/verifyToken.js');

module.exports = (app) => {
  app.get('/api/users/me', requestLogger, verifyToken, user.get);

  app.patch('/api/users/me', requestLogger, verifyToken, user.update);

  app.delete('/api/users/me', requestLogger, verifyToken, user.delete);
};
