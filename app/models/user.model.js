const mongoose = require('mongoose');
const {Schema} = mongoose;

const UserSchema = new Schema(
    {
      _id: String,
      username: String,
      password: String,
    },
    {
      timestamps: {
        createdAt: 'createdDate',
        updatedAt: false,
      },
      versionKey: false,
    });

module.exports = mongoose.model('User', UserSchema);
