const mongoose = require('mongoose');
const {Schema} = mongoose;

const NoteSchema = new Schema(
    {
      _id: String,
      userId: String,
      completed: Boolean,
      text: String,
    },
    {
      timestamps: {
        createdAt: 'createdDate',
        updatedAt: false,
      },
      versionKey: false,
    });

module.exports = mongoose.model('Note', NoteSchema);
