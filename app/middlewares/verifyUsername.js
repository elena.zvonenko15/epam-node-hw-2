const User = require('../models/user.model.js');

const verifyUsername = (req, res, next) => {
  User.findOne({
    username: req.body.username,
  }).exec((err, user) => {
    if (err) {
      return res.status(500).send({message: err});
    }
    if (user) {
      return res.status(400).send({message: 'Username is already in use'});
    }
    next();
  });
};

module.exports = verifyUsername;
