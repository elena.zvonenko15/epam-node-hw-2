const mongoose = require('mongoose');
const Note = require('../models/note.model.js');

exports.getAll = async (req, res) => {
  const offset = req.query.offset === undefined ? 0 : +req.query.offset;
  const limit = req.query.limit === undefined ? 0 : +req.query.limit;
  const userId = req.user._id;
  const notes = await Note.find({userId}).skip(offset).limit(limit);
  const count = notes.length;

  try {
    if (!count) {
      return res.status(400).send({
        message: 'Notes not found',
      });
    }
    res.send({
      offset,
      limit,
      count,
      notes,
    });
  } catch (err) {
    res.status(500).send({
      message: err.message || 'Some error occurred while retrieving notes',
    });
  }
};

exports.create = async (req, res) => {
  if (!req.body.text) {
    return res.status(400).send({
      message: 'Note text can not be empty',
    });
  }

  const note = new Note({
    _id: new mongoose.Types.ObjectId().toHexString(),
    userId: req.user._id,
    completed: false,
    text: req.body.text,
  });

  try {
    await note.save();
    res.send({message: 'Success'});
  } catch (err) {
    res.status(500).send({
      message: err.message || 'Some error occurred while creating the note',
    });
  }
};

exports.getOne = async (req, res) => {
  const note = await Note.findById(req.params.noteId);
  try {
    if (!note) {
      return res.status(400).send({
        message: `Note with id ${req.params.noteId} not found`,
      });
    }
    res.send({note: note});
  } catch (err) {
    return res.status(500).send({
      message: `Error retrieving note with id ${req.params.noteId}`,
    });
  }
};

exports.update = async (req, res) => {
  if (!req.body.text) {
    return res.status(400).send({
      message: 'Note text can not be empty',
    });
  }

  const note = await Note.findByIdAndUpdate(req.params.noteId, {
    text: req.body.text,
  }, {new: true});
  try {
    if (!note) {
      return res.status(400).send({
        message: `Note with id ${req.params.noteId} not found`,
      });
    }
    res.send({message: 'Success'});
  } catch (err) {
    return res.status(500).send({
      message: `Error updating note with id ${req.params.noteId}`,
    });
  }
};

exports.toggle = async (req, res) => {
  const note = await Note.findByIdAndUpdate(req.params.noteId,
      [{$set: {completed: {$eq: [false, '$completed']}}}],
      {timestamps: false});
  try {
    if (!note) {
      return res.status(400).send({
        message: `Note with id ${req.params.noteId} not found`,
      });
    }
    res.send({message: 'Success'});
  } catch {
    return res.status(500).send({
      message: `Error checking note with id ${req.params.noteId}`,
    });
  }
};

exports.delete = async (req, res) => {
  const note = await Note.findByIdAndRemove(req.params.noteId);
  try {
    if (!note) {
      return res.status(400).send({
        message: `Note with id ${req.params.noteId} not found`,
      });
    }
    res.send({message: 'Success'});
  } catch {
    return res.status(500).send({
      message: `Error deleting note with id ${req.params.noteId}`,
    });
  }
};
