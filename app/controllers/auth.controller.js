const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model.js');
const config = require('../config/auth.config.js');

exports.register = async (req, res) => {
  if (!req.body.username || !req.body.password) {
    return res.status(400).send({
      message: 'You need to enter the username and password',
    });
  }

  const user = new User({
    _id: new mongoose.Types.ObjectId().toHexString(),
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 5),
  });

  try {
    await user.save();
    res.send({message: 'Success'});
  } catch (err) {
    return res.status(500).send({
      message: 'Error registering user',
    });
  }
};

exports.login = async (req, res) => {
  if (!req.body.username || !req.body.password) {
    return res.status(400).send({
      message: 'You need to enter the username and password',
    });
  }
  const user = await User.findOne({username: req.body.username}, 'password');
  try {
    if (!user) {
      return res.status(400).send({
        message: `User ${req.body.username} not found`,
      });
    }

    const passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password,
    );
    if (!passwordIsValid) {
      return res.status(400).send({
        message: 'Invalid password',
      });
    }

    const token = jwt.sign({_id: user._id}, config.secret, {expiresIn: 86400});
    res.send({
      message: 'Success',
      jwt_token: token,
    });
  } catch (err) {
    return res.status(500).send({
      message: 'Error logging user',
    });
  }
};
