const bcrypt = require('bcrypt');
const User = require('../models/user.model.js');

exports.get = async (req, res) => {
  const user = await User.findById(req.user._id);
  try {
    if (!user) {
      return res.status(400).send({
        message: 'User not found',
      });
    }
    res.send({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },

    });
  } catch (err) {
    return res.status(500).send({
      message: 'Error retrieving user',
    });
  }
};

exports.update = async (req, res) => {
  if (!req.body.oldPassword || !req.body.newPassword) {
    return res.status(400).send({
      message: 'You need to enter old and new password',
    });
  }

  if (req.body.oldPassword === req.body.newPassword) {
    return res.status(400).send({
      message: 'The new password must be different from the old one',
    });
  }

  const user = await User.findById(req.user._id);
  try {
    if (!user) {
      return res.status(400).send({
        message: 'User not found',
      });
    }

    const passwordIsValid = bcrypt.compareSync(
        req.body.oldPassword,
        user.password,
    );
    if (!passwordIsValid) {
      return res.status(400).send({
        message: 'Invalid old password',
      });
    }

    await User.findByIdAndUpdate(req.user._id,
        {password: bcrypt.hashSync(req.body.newPassword, 5)},
        {new: true});
    res.send({message: 'Success'});
  } catch (err) {
    return res.status(500).send({
      message: 'Error updating user',
    });
  }
};

exports.delete = async (req, res) => {
  const user = await User.findByIdAndRemove(req.user._id);
  try {
    if (!user) {
      return res.status(400).send({
        message: 'User not found',
      });
    }
    res.send({message: 'Success'});
  } catch {
    return res.status(500).send({
      message: 'Error deleting user',
    });
  }
};
