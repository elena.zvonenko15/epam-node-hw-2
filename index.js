const express = require('express');
const port = process.env.PORT || 8080;
const app = express();

app.use(express.json());

const mongoose = require('mongoose');
const dbConfig = require('./app/config/database.config.js');

mongoose.connect(dbConfig.url, {
  useNewUrlParser: true,
}).then(() => {
  console.log('Successfully connected to the database');
}).catch((err) => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

app.get('/', (req, res) => {
  res.send('GET request to homepage');
});

require('./app/routes/auth.routes.js')(app);
require('./app/routes/user.routes.js')(app);
require('./app/routes/note.routes.js')(app);

app.use(express.static(__dirname + '/public'));

app.listen(port, () => {
  console.log(`Server has been started on port ${port}`);
});


